# LIS4368 - Advanced Web Applications Spring 2016 P1

## Martin Sung (The Reigning CORN-HOLE CHAMP)

### Assignment Requirements:

#### Deliverables:

1. Open index.jsp and review code:
a. Suitably modify meta tags
b. Change title, navigation links, and header tags appropriately
c. Add form controls to match attributes of customer entity
d. Add the following jQuery validation and regular expressions-- as per the entity attribute
requirements (and screenshots below):
i. *All* input fields, except Notes are required
ii. Use min/max jQuery validation
iii. Use regexp to only allow appropriate characters for each control:
cus_fname, cus_lname, cus_street, cus_city, cus_state, cus_zip, cus_phone, cus_email,
cus_balance, cus_total_sales, cus_notes
fname, lname: provided
street, city:
no more than 30 characters
Street: must only contain letters, numbers, commas, or periods
City: must only contain letters, numbers
state:
must be 2 characters
must only contain letters
zip:
must be between 5 and 9 characters, inclusive
must only contain numbers
phone:
must be 10 characters, including area code
must only contain numbers
email: provided (also, see below: Lesson 7)
balance, total_sales:
no more than 6 digits, including decimal point
can only contain numbers, and decimal point (if used)
iv. *After* testing jQuery validation, use HTML5 property to limit the number of characters for
each control
v. Research what the following validation code does:
valid: 'fa fa-check',
invalid: 'fa fa-times',
validating: 'fa fa-refresh'
e. Use git to push *all* p1 files and changes to your remote course Bitbucket repo

1. Provide Bitbucket read-only access to p1 repo, *must* include README.md, using Markdown
syntax.
2. Blackboard Links: p1 Bitbucket repo



### P1 Images


![P1 Requirements](img/p1_1.png)
![P1 Requirements](img/p1_2.png)